//https://ullisroboterseite.de/android-AI2-MQTT-en.html
//inventor mqtt extension
 

#include <PubSubClient.h> // Importa a Biblioteca PubSubClient

#include <Adafruit_NeoPixel.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <Ticker.h>  

//https://www.circuitschools.com/change-wifi-credentials-of-esp8266-without-uploading-code-again/

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266WebServer.h>
#include <EEPROM.h>

//Variables
int i = 0;
int statusCode;
const char* ssid = "text";
const char* passphrase = "text";
String st;
String content;
 
 
//Function Declaration
bool testWifi(void);
void launchWeb(void);
void setupAP(void);
 
//--------Establishing Local server at port 80 whenever required
ESP8266WebServer server(80);



char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

// Define NTP Client to get time
const long utcOffsetInSeconds = -3 * 3600; //GMT -3
WiFiUDP ntpUDP;
NTPClient timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds,60000);


#define PIN 14
// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
//goku
//Adafruit_NeoPixel strip = Adafruit_NeoPixel(30, PIN,  NEO_GRB + NEO_KHZ800);
//Adelaide
Adafruit_NeoPixel strip = Adafruit_NeoPixel(40, PIN,  NEO_GRB + NEO_KHZ800);


 
//defines:
//defines de id mqtt e tópicos para publicação e subscribe
#define TOPICO_EFEITO_NOW "caguto/goku/efeito"     //tópico MQTT de escuta
#define TOPICO_INTENSIDADE_NOW "caguto/goku/intensidade"     //tópico MQTT de escuta
#define TOPICO_VELOCIDADE_NOW "caguto/goku/velocidade"     //tópico MQTT de escuta
#define TOPICO_EFEITO_WEEK "caguto/goku/efeitosemana"     //tópico MQTT de escuta
#define TOPICO_INTENSIDADE_WEEK "caguto/goku/intensidadesemana"     //tópico MQTT de escuta
#define TOPICO_VELOCIDADE_WEEK "caguto/goku/velocidadesemana"     //tópico MQTT de escuta
#define TOPICO_EFEITO_WEEKEND "caguto/goku/efeitofds"     //tópico MQTT de escuta
#define TOPICO_INTENSIDADE_WEEKEND "caguto/goku/intensidadefds"     //tópico MQTT de escuta
#define TOPICO_VELOCIDADE_WEEKEND "caguto/goku/velocidadefds"     //tópico MQTT de escuta

#define TOPICO_ON_WEEK "caguto/goku/onsemana"     //tópico MQTT de escuta
#define TOPICO_OFF_WEEK "caguto/goku/offsemana"     //tópico MQTT de escuta
#define TOPICO_ON_WEEKEND "caguto/goku/onfds"     //tópico MQTT de escuta
#define TOPICO_OFF_WEEKEND "caguto/goku/offfds"     //tópico MQTT de escuta

#define TOPICO_PUBLISH   "caguto/goku/pub"    //tópico MQTT de envio de informações para Broker
                                                   //IMPORTANTE: recomendamos fortemente alterar os nomes
                                                   //            desses tópicos. Caso contrário, há grandes
                                                   //            chances de você controlar e monitorar o NodeMCU
                                                   //            de outra pessoa.
#define ID_MQTT  "caguto/goku/"     //id mqtt (para identificação de sessão)
                               //IMPORTANTE: este deve ser único no broker (ou seja, 
                               //            se um client MQTT tentar entrar com o mesmo 
                               //            id de outro já conectado ao broker, o broker 
                               //            irá fechar a conexão de um deles).
                                
 
//defines - mapeamento de pinos do NodeMCU
//#define LED 2
#define LED LED_BUILTIN
#define BTN_PIN 0 //GPIO 0 (Flash Button)
 
  
// MQTT
//public brokers
//https://moxd.io/2015/10/17/public-mqtt-brokers/

//base em protugues
//https://www.curtocircuito.com.br/blog/Categoria%20IoT/monitoramento-e-controle-por-aplicativo-mqtt

//const char* BROKER_MQTT = "test.mosquitto.org"; //URL do broker MQTT que se deseja utilizar funciona
//const char* BROKER_MQTT = "broker.hivemq.com";  //não funciona
int BROKER_PORT = 1883; // Porta do Broker MQTT

const char* BROKER_MQTT = "broker.mqtt-dashboard.com";  //não funciona
//http://www.hivemq.com/demos/websocket-client/


 
 
//Variáveis e objetos globais
WiFiClient espClient; // Cria o objeto espClient
PubSubClient MQTT(espClient); // Instancia o Cliente MQTT passando o objeto espClient


int cor=0;
int intensidade=10;
int velocidade = 0;
float lvl = 1.0;
int corsemana=0;
int intensidadesemana=10;
int velocidadesemana = 0;
int corfds=0;
int intensidadefds=10;
int velocidadefds = 0;

int onsemana = 0;
int onhoursemana = 0;
int onminsemana = 0;
int onfds = 0;
int onhourfds = 0;
int onminfds = 0;
int offsemana = 0;
int offhoursemana = 0;
int offminsemana = 0;
int offfds = 0;
int offhourfds = 0;
int offminfds = 0;

int today = -1;
int now = 0;
int todayonfds = 10000;
int todayofffds = 10000;
int todayonsemana = 10000;
int todayoffsemana = 10000;

unsigned long connectionMillis=0;
unsigned long dateMillis=0;
unsigned long ledMillis=0;

//Prototypes
void initSerial();
void initWiFi();
void initMQTT();
void reconectWiFi(); 
void mqtt_callback(char* topic, byte* payload, unsigned int length);
void VerificaConexoesWiFIEMQTT(void);
void createWebServer();
void InitOutput(void);

void main_loop();

void colorWipe(uint32_t c, uint8_t wait);
void blank() ;
void red();
void blue();
void green();
void yellow() ;
void white();
uint32_t Wheel(byte WheelPos);
void color(uint32 idx, uint32_t c, uint8_t wait) ;

void setup()
{


 
  Serial.begin(115200); //Initializing if(DEBUG)Serial Monitor
  Serial.println();

  Serial.println("Oi, eu sou goku!");
    
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  colorWipe(strip.Color(255, 0, 0), 50); // RED
  blank();
  
  Serial.println("Disconnecting previously connected WiFi");
  WiFi.disconnect();
  EEPROM.begin(512); //Initializing EEPROM
  delay(10);
  pinMode(LED_BUILTIN, OUTPUT);
  Serial.println();
  Serial.println();
  Serial.println("Startup");
 
  // Read eeprom for ssid and password
  Serial.println("Reading EEPROM ssid");
 
  String esid;
  for (int i = 0; i < 32; ++i)
  {
    esid += char(EEPROM.read(i));
  }
  Serial.println();
  Serial.print("SSID: ");
  Serial.println(esid);
  Serial.println("Reading EEPROM pass");
 
  String epass = "";
  for (int i = 32; i < 96; ++i)
  {
    epass += char(EEPROM.read(i));
  }
  Serial.print("PASS: ");
  Serial.println(epass);
 
 
  WiFi.begin(esid.c_str(), epass.c_str());
  if (testWifi())
  {
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());   //You can get IP address assigned to ESP
    return;
  }
  else
  {
    Serial.println("Turning the HotSpot On");
    launchWeb();
    setupAP();// Setup accesspoint or HotSpot
  }
 
  Serial.println();
  Serial.println("Waiting.");
  
  while ((WiFi.status() != WL_CONNECTED))
  {
    Serial.print(".");
    delay(100);
    server.handleClient();
  }
 
}
 
void init_lumi() 
{

    //initWiFi();
    initMQTT();
  
    Serial.println("Waiting for first NTP update");
    while(!timeClient.update())
    {
        Serial.print(".");
        timeClient.forceUpdate();
        delay(500);
    }

  Serial.print(daysOfTheWeek[timeClient.getDay()]);
  Serial.print(", ");
  Serial.print(timeClient.getHours());
  Serial.print(":");
  Serial.print(timeClient.getMinutes());
  Serial.print(":");
  Serial.println(timeClient.getSeconds());

  
  colorWipe(strip.Color(255, 255, 255), 50); // White RGBW

  blank();
}
  
  
//Função: inicializa parâmetros de conexão MQTT(endereço do 
//        broker, porta e seta função de callback)
//Parâmetros: nenhum
//Retorno: nenhum
void initMQTT() 
{
    MQTT.setServer(BROKER_MQTT, BROKER_PORT);   //informa qual broker e porta deve ser conectado
    MQTT.setCallback(mqtt_callback);            //atribui função de callback (função chamada quando qualquer informação de um dos tópicos subescritos chega)
}

String getValue(String data, char separator, int index)
{
    int found = 0;
    int strIndex[] = { 0, -1 };
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == separator || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i+1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}


bool flag_mqtt = false;
//Função: função de callback 
//        esta função é chamada toda vez que uma informação de 
//        um dos tópicos subescritos chega)
//Parâmetros: nenhum
//Retorno: nenhum
void mqtt_callback(char* topic, byte* payload, unsigned int length) 
{
    String msg;
    Serial.print("MQTT Calback: ");
    //obtem a string do payload recebido
    for(int i = 0; i < length; i++) 
    {
       char c = (char)payload[i];
       msg += c;
    }

      Serial.print("Topic: ");
      Serial.print(topic);   
      Serial.print(" = ");
      Serial.println(msg);

    if (strcmp(topic, "caguto/goku/efeito") == 0) 
    {
         cor=msg.toInt();
         flag_mqtt = true;
    }
    else if (strcmp(topic, "caguto/goku/intensidade") == 0) 
    {
         intensidade=msg.toInt();
         lvl = ((float)intensidade)/10.0;
         flag_mqtt = true;
    }
    else if (strcmp(topic, "caguto/goku/velocidade") == 0) 
    {
         velocidade=msg.toInt();
         flag_mqtt = true;
    }
    else if (strcmp(topic, "caguto/goku/efeitosemana") == 0) 
    {
         corsemana=msg.toInt();
    }
    else if (strcmp(topic, "caguto/goku/intensidadesemana") == 0) 
    {
         intensidadesemana=msg.toInt();
         lvl = ((float)intensidade)/10.0;
    }
    else if (strcmp(topic, "caguto/goku/velocidadesemana") == 0) 
    {
         velocidadesemana=msg.toInt();
    }
    else if (strcmp(topic, "caguto/goku/efeitofds") == 0) 
    {
         corfds=msg.toInt();
    }
    else if (strcmp(topic, "caguto/goku/intensidadefds") == 0) 
    {
         intensidadefds=msg.toInt();
         lvl = ((float)intensidade)/10.0;
    }
    else if (strcmp(topic, "caguto/goku/velocidadefds") == 0) 
    {
         velocidadefds=msg.toInt();
    }    
    else if (strcmp(topic, "caguto/goku/onfds") == 0) 
    {
      
         String str1 = getValue(msg, ':', 0);
         String str2 = getValue(msg, ':', 1);
         onhourfds=str1.toInt();
         onminfds=str2.toInt();
         onfds = onhourfds*60+onminfds;
         if(now<offfds)
          todayonfds = onfds;
         else
          todayonfds = 10000;
    }
    else if (strcmp(topic, "caguto/goku/offfds") == 0) 
    {
         
         String str1 = getValue(msg, ':', 0);
         String str2 = getValue(msg, ':', 1);
         offhourfds=str1.toInt();
         offminfds=str2.toInt();
         offfds = offhourfds*60+offminfds;
         if(now<offfds)
          todayofffds = offfds;
         else
          todayofffds = 10000;

    }
    else if (strcmp(topic, "caguto/goku/onsemana") == 0) 
    {
         String str1 = getValue(msg, ':', 0);
         String str2 = getValue(msg, ':', 1);
         onhoursemana=str1.toInt();
         onminsemana=str2.toInt();
         onsemana = onhoursemana*60+onminsemana;
         if(now<onsemana)
          todayonsemana = offsemana;
         else
          todayonsemana = 10000;
    }
    else if (strcmp(topic, "caguto/goku/offsemana") == 0) 
    {

         String str1 = getValue(msg, ':', 0);
         String str2 = getValue(msg, ':', 1);
         offhoursemana=str1.toInt();
         offminsemana=str2.toInt();
         offsemana = offhoursemana*60+offminsemana;
         if(now<offsemana)
          todayoffsemana = offsemana;
         else
          todayoffsemana = 10000;
          
    }


}
//Função: reconecta-se ao broker MQTT (caso ainda não esteja conectado ou em caso de a conexão cair)
//        em caso de sucesso na conexão ou reconexão, o subscribe dos tópicos é refeito.
//Parâmetros: nenhum
//Retorno: nenhum
void reconnectMQTT() 
{
    if (!MQTT.connected()) 
    {
        Serial.println("Tentando se conectar ao Broker MQTT: ");
        Serial.println(BROKER_MQTT);
        if (MQTT.connect(ID_MQTT)) 
        {
            Serial.println("Conectado com sucesso ao broker MQTT!");
            MQTT.subscribe(TOPICO_EFEITO_NOW); 
            MQTT.subscribe(TOPICO_INTENSIDADE_NOW);
            MQTT.subscribe(TOPICO_VELOCIDADE_NOW);
            MQTT.subscribe(TOPICO_EFEITO_WEEK); 
            MQTT.subscribe(TOPICO_INTENSIDADE_WEEK);
            MQTT.subscribe(TOPICO_VELOCIDADE_WEEK);
            MQTT.subscribe(TOPICO_EFEITO_WEEKEND); 
            MQTT.subscribe(TOPICO_INTENSIDADE_WEEKEND);
            MQTT.subscribe(TOPICO_VELOCIDADE_WEEKEND);
            MQTT.subscribe(TOPICO_ON_WEEKEND);
            MQTT.subscribe(TOPICO_OFF_WEEKEND);
            MQTT.subscribe(TOPICO_ON_WEEK);
            MQTT.subscribe(TOPICO_OFF_WEEK);
        } 
        else
        {
            Serial.println("Falha ao reconectar no broker.");
            Serial.println("Havera nova tentativa de conexao");
        }
    }
}
  

 
//Função: verifica o estado das conexões WiFI e ao broker MQTT. 
//        Em caso de desconexão (qualquer uma das duas), a conexão
//        é refeita.
//Parâmetros: nenhum
//Retorno: nenhum
void VerificaConexoesWiFIEMQTT(void)
{
    if (!MQTT.connected()) 
        reconnectMQTT(); //se não há conexão com o Broker, a conexão é refeita
     
     //reconectWiFi(); //se não há conexão com o WiFI, a conexão é refeita
}
 
//Função: Trata LEDS
//Parâmetros: nenhum
//Retorno: nenhum

int estado = 0;

void TrataLeds(void)
{

     //MQTT.publish(TOPICO_PUBLISH, "W",true);

    switch(estado){
      case 0:
        if(flag_mqtt==true){
          estado = 1;
          flag_mqtt = false;
        }
        case 1:
        {
          if(cor<=1){
            blank(); 
            estado = 0;
          }
          else if(cor==2){
            red();
            estado = 0;
          }
          else if(cor==3){
            blue(); 
            estado = 0;
          }
          else if(cor==4){
            green(); 
            estado = 0;
          }
          else if(cor==5){
            yellow(); 
            estado = 0;
          }
          else if(cor==6){
            white(); 
            estado = 0;
          }
          else if(cor==7){
            static int i = 0;
            for(i=0;i<velocidade+1;i++){ 
              strip.setPixelColor(random(strip.numPixels()),strip.Color(random(200*lvl,255*lvl), random(30*lvl), random(30*lvl)));
            } 
            strip.show();
          }
          else if(cor==8){
            static int i = 0;
            for(i=0;i<velocidade+1;i++){ 
              strip.setPixelColor(random(strip.numPixels()),strip.Color(random(30*lvl), random(200*lvl,255*lvl), random(30*lvl)));
            } 
            strip.show();
          }
          else if(cor==9){
            static int i = 0;
            for(i=0;i<velocidade+1;i++){ 
              strip.setPixelColor(random(strip.numPixels()),strip.Color(random(30*lvl), random(30*lvl), random(200*lvl,255*lvl)));
            } 
            strip.show(); 
          }
          else if(cor==10){
            static int i = 0;
            for(i=0;i<velocidade+1;i++){ 
              strip.setPixelColor(random(strip.numPixels()),strip.Color(random(200*lvl,255*lvl), random(80*lvl,120*lvl), random(30*lvl)));
            } 
            strip.show(); 
          }
          else if(cor==11){
            static int rainbow_cor = 0;
            static int rainbow_led = 0;
            static int timer = velocidade;
            if(rainbow_led>=strip.numPixels()) rainbow_led = 0;
            strip.setPixelColor(rainbow_led, Wheel(byte((rainbow_cor+rainbow_led) & 255)));
            rainbow_led++;
            if(timer--<=0){
              timer = velocidade;
              rainbow_cor++;
              if(rainbow_cor>=255) rainbow_cor = 0;
            }
            strip.show();
          } 
          else if(cor==12){
            static int rainbow_cor = 0;
            static int rainbow_led = 0;
            static int rainbow_3th_led = 3;
            static int timer = velocidade;
            if(timer--<=0){
              timer = velocidade;
              if( rainbow_cor > 255 ) rainbow_cor = 0;   // cycle all 256 colors in the wheel
              if( rainbow_led >= strip.numPixels()-1 ) {
                rainbow_led = 0; 
              }
              if( rainbow_3th_led >= strip.numPixels()-1 ) {
                rainbow_3th_led = 0; 
              }
              strip.setPixelColor(rainbow_led, Wheel( byte((rainbow_led+rainbow_cor) % 255)));    //turn every third pixel on
              strip.show();
              strip.setPixelColor(rainbow_3th_led , 0);        //turn every third pixel off
              rainbow_cor++;
              rainbow_led++;
              rainbow_3th_led++;
            }
          }

          else if(cor==13){
            static int timer = velocidade;
            if(timer--<=0){
              timer = velocidade;
              color(random(strip.numPixels()),strip.Color(random(255*lvl), random(255*lvl), random(255*lvl)),0); 
            }
          }
          else if(cor==14){
            static int i = 0;
            blank();
            for(i=0;i<velocidade+1;i++){ 
              strip.setPixelColor(random(strip.numPixels()),strip.Color((255*lvl), (255*lvl), (255*lvl)));
            } 
            strip.show();
          }
        }      
    } 
}
 
//Função: inicializa o output em nível lógico baixo
//Parâmetros: nenhum
//Retorno: nenhum
void InitOutput(void)
{
    //IMPORTANTE: o Led já contido na placa é acionado com lógica invertida (ou seja,
    //enviar HIGH para o output faz o Led apagar / enviar LOW faz o Led acender)
    pinMode(LED, OUTPUT);
    digitalWrite(LED, HIGH);          
}
 


void loop() {
  static int main_state = 0;
  static boolean flag_wifi = false;
  if ((WiFi.status() == WL_CONNECTED))
  {
    flag_wifi = true;
    switch(main_state){
      case 0:
        init_lumi();
        main_state = 1;
        break;
       case 1:
        main_loop();
        main_state = 1;
        break;
    }
 
  }
  else
  {
    if (flag_wifi==true){
      flag_wifi = false;
      Serial.println("Wifi Connection Problem!");
    }
  }
 
}
 
//programa principal
void main_loop() 
{   

    //garante funcionamento das conexões WiFi e ao broker MQTT
    if(millis() - connectionMillis > 1000){
      connectionMillis = millis(); 
      VerificaConexoesWiFIEMQTT();
    }
    
     if(millis() - dateMillis > 10000){
        dateMillis = millis(); 
        now = timeClient.getHours()*60+timeClient.getMinutes();
      
        if(timeClient.getDay() != today){
          today = timeClient.getDay();

          todayonfds = onfds;
          todayofffds = offfds;
          todayonsemana = onsemana;
          todayoffsemana = offsemana;
          
        }
        
       
        //fds
        if(timeClient.getDay()==0 || timeClient.getDay()==6 )
        {
          if((corfds != 1) && (cor == 1))
          {
            if(now >= todayonfds)
            {
              cor = corfds;
              intensidade = intensidadefds;
              todayonfds = 10000;
              Serial.println("Ligando FDS");
              Serial.println();
              Serial.print("now:");
              Serial.println(now);
              Serial.print("onfds:");
              Serial.println(todayonfds);
              Serial.print("offds:");
              Serial.println(todayofffds);
              Serial.print("onsemana:");
              Serial.println(todayonsemana);
              Serial.print("offsemana:");
              Serial.println(todayoffsemana);

              Serial.print("cor:");
              Serial.println(cor);
              Serial.print("corsemana:");
              Serial.println(corsemana);
              Serial.print("corfds:");
              Serial.println(corfds);
              Serial.print("day:");
              Serial.println(timeClient.getDay());
            }
          }
          else if((corfds != 1)  && (cor != 1))
          {
            if(now >= todayofffds)
            {
              cor = 1;
              todayofffds = 10000;
              Serial.println("Desligando FDS");
              Serial.println();
              Serial.print("now:");
              Serial.println(now);
              Serial.print("onfds:");
              Serial.println(todayonfds);
              Serial.print("offds:");
              Serial.println(todayofffds);
              Serial.print("onsemana:");
              Serial.println(todayonsemana);
              Serial.print("offsemana:");
              Serial.println(todayoffsemana);

              Serial.print("cor:");
              Serial.println(cor);
              Serial.print("corsemana:");
              Serial.println(corsemana);
              Serial.print("corfds:");
              Serial.println(corfds);
              Serial.print("day:");
              Serial.println(timeClient.getDay());
            }
          }
        }
        else
        {
          if((corsemana != 1) && (cor == 1))
          {
            if(now >= todayonsemana)
            {
              cor = corsemana;
              intensidade = intensidadesemana;
              todayonsemana = 10000;
              Serial.println("Ligando Semana");
              Serial.println();
              Serial.print("now:");
              Serial.println(now);
              Serial.print("onfds:");
              Serial.println(todayonfds);
              Serial.print("offds:");
              Serial.println(todayofffds);
              Serial.print("onsemana:");
              Serial.println(todayonsemana);
              Serial.print("offsemana:");
              Serial.println(todayoffsemana);

              Serial.print("cor:");
              Serial.println(cor);
              Serial.print("corsemana:");
              Serial.println(corsemana);
              Serial.print("corfds:");
              Serial.println(corfds);
              Serial.print("day:");
              Serial.println(timeClient.getDay());
            }
          }
           else if((corsemana != 1) && (cor != 1))
           {
            if(now >= todayoffsemana)
            {
              cor = 1;
              todayoffsemana = 10000;
              Serial.println("Desligando Semana");
              Serial.println();
              Serial.print("now:");
              Serial.println(now);
              Serial.print("onfds:");
              Serial.println(todayonfds);
              Serial.print("offds:");
              Serial.println(todayofffds);
              Serial.print("onsemana:");
              Serial.println(todayonsemana);
              Serial.print("offsemana:");
              Serial.println(todayoffsemana);

              Serial.print("cor:");
              Serial.println(cor);
              Serial.print("corsemana:");
              Serial.println(corsemana);
              Serial.print("corfds:");
              Serial.println(corfds);
              Serial.print("day:");
              Serial.println(timeClient.getDay());
            }
          }

        }
    }


    if(millis() - ledMillis > 10){
      ledMillis = millis(); 
      TrataLeds();
    }
 
    //keep-alive da comunicação com broker MQTT
    MQTT.loop();
}



// Fill the dots one after the other with a color
void blank() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show(); // Initialize all pixels to 'off'
}


// Fill the dots one after the other with a color
void color(uint32 idx, uint32_t c, uint8_t wait) {
    strip.setPixelColor(idx, c);
    strip.show();
   // delay(wait);
}


void red() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255*lvl, 0, 0));
  }
  strip.show(); // Initialize all pixels to 'off'
}

void blue() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 255*lvl));
  }
  strip.show(); // Initialize all pixels to 'off'
}

void green() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(0, 255*lvl, 0));
  }
  strip.show(); // Initialize all pixels to 'off'
}

void yellow() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255*lvl, 100*lvl, 0));
  }
  strip.show(); // Initialize all pixels to 'off'
}


void white() {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, strip.Color(255*lvl, 255*lvl, 255*lvl));
  }
  strip.show(); // Initialize all pixels to 'off'
}


// Fill the dots one after the other with a color
void colorWipe(uint32_t c, uint8_t wait) {
  for(uint16_t i=0; i<strip.numPixels(); i++) {
    strip.setPixelColor(i, c*lvl);
    strip.show();
    //delay(wait);
    blank();
  }
}

void rainbow(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256; j++) {
    for(i=0; i<strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(byte((i+j) & 255)));
    }
    strip.show();
    //delay(wait);
    //delay(wait);
  }
  blank();
}

// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t wait) {
  uint16_t i, j;

  for(j=0; j<256*5; j++) { // 5 cycles of all colors on wheel
    for(i=0; i< strip.numPixels(); i++) {
      strip.setPixelColor(i, Wheel(byte(((i * 256 / strip.numPixels()) + j) & 255)));
    }
    strip.show();
    //delay(wait);
  }
}

//Theatre-style crawling lights.
void theaterChase(uint32_t c, uint8_t wait) {
  for (int j=0; j<10; j++) {  //do 10 cycles of chasing
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, c);    //turn every third pixel on
      }
      strip.show();

      //delay(wait);

      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

//Theatre-style crawling lights with rainbow effect
void theaterChaseRainbow(uint8_t wait) {
  for (int j=0; j < 256; j++) {     // cycle all 256 colors in the wheel
    for (int q=0; q < 3; q++) {
      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, Wheel(byte( (i+j) % 255)));    //turn every third pixel on
      }
      strip.show();


      for (uint16_t i=0; i < strip.numPixels(); i=i+3) {
        strip.setPixelColor(i+q, 0);        //turn every third pixel off
      }
    }
  }
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  WheelPos = 255 - WheelPos;
  if(WheelPos < 85) {
    return strip.Color((255 - WheelPos * 3)*lvl, 0, (WheelPos * 3)*lvl);
  }
  if(WheelPos < 170) {
    WheelPos -= 85;
    return strip.Color(0, (WheelPos * 3)*lvl, (255 - WheelPos * 3)*lvl);
  }
  WheelPos -= 170;
  return strip.Color((WheelPos * 3)*lvl, (255 - WheelPos * 3)*lvl, 0);
}


//Functions used for saving WiFi credentials and to connect to it which you do not need to change 
bool testWifi(void)
{
  int c = 0;
  Serial.println("Waiting for WiFi to connect");
  while ( c < 20 ) {
    if (WiFi.status() == WL_CONNECTED)
    {
      return true;
    }
    delay(500);
    Serial.print("*");
    c++;
  }
  Serial.println("");
  Serial.println("Connection timed out, opening AP or Hotspot");
  return false;
}
 
void launchWeb()
{
  Serial.println("");
  if (WiFi.status() == WL_CONNECTED)
    Serial.println("WiFi connected");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("SoftAP IP: ");
  Serial.println(WiFi.softAPIP());
  createWebServer();
  // Start the server
  server.begin();
  Serial.println("Server started");
}
 
void setupAP(void)
{
  WiFi.mode(WIFI_STA);
  WiFi.disconnect();
  delay(100);
  int n = WiFi.scanNetworks();
  Serial.println("scan completed");
  if (n == 0)
    Serial.println("No WiFi Networks found");
  else
  {
    Serial.print(n);
    Serial.println(" Networks found");
    for (int i = 0; i < n; ++i)
    {
      // Print SSID and RSSI for each network found
      Serial.print(i + 1);
      Serial.print(": ");
      Serial.print(WiFi.SSID(i));
      Serial.print(" (");
      Serial.print(WiFi.RSSI(i));
      Serial.print(")");
      Serial.println((WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*");
      delay(10);
    }
  }
  Serial.println("");
  st = "<ol>";
  for (int i = 0; i < n; ++i)
  {
    // Print SSID and RSSI for each network found
    st += "<li>";
    st += WiFi.SSID(i);
    st += " (";
    st += WiFi.RSSI(i);
 
    st += ")";
    st += (WiFi.encryptionType(i) == ENC_TYPE_NONE) ? " " : "*";
    st += "</li>";
  }
  st += "</ol>";
  delay(100);
  WiFi.softAP("CAGUTO GOKU", "");
  Serial.println("Initializing_Wifi_accesspoint");
  launchWeb();
  Serial.println("over");
}
 
void createWebServer()
{
 {
    server.on("/", []() {
 
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
      content = "<!DOCTYPE HTML>\r\n<html>OI, EU SOU O GOKU! Preciso de sua ajuda... ";
      content += "<form action=\"/scan\" method=\"POST\"><input type=\"submit\" value=\"scan\"></form>";
      content += ipStr;
      content += "<p>";
      content += st;
      content += "</p><form method='get' action='setting'><label>SSID: </label><input name='ssid' length=32><input name='pass' length=64><input type='submit'></form>";
      content += "</html>";
      server.send(200, "text/html", content);
    });
    server.on("/scan", []() {
      //setupAP();
      IPAddress ip = WiFi.softAPIP();
      String ipStr = String(ip[0]) + '.' + String(ip[1]) + '.' + String(ip[2]) + '.' + String(ip[3]);
 
      content = "<!DOCTYPE HTML>\r\n<html>go back";
      server.send(200, "text/html", content);
    });
 
    server.on("/setting", []() {
      String qsid = server.arg("ssid");
      String qpass = server.arg("pass");
      if (qsid.length() > 0 && qpass.length() > 0) {
        Serial.println("clearing eeprom");
        for (int i = 0; i < 96; ++i) {
          EEPROM.write(i, 0);
        }
        Serial.println(qsid);
        Serial.println("");
        Serial.println(qpass);
        Serial.println("");
 
        Serial.println("writing eeprom ssid:");
        for (int i = 0; i < qsid.length(); ++i)
        {
          EEPROM.write(i, qsid[i]);
          Serial.print("Wrote: ");
          Serial.println(qsid[i]);
        }
        Serial.println("writing eeprom pass:");
        for (int i = 0; i < qpass.length(); ++i)
        {
          EEPROM.write(32 + i, qpass[i]);
          Serial.print("Wrote: ");
          Serial.println(qpass[i]);
        }
        EEPROM.commit();
 
        content = "{\"Success\":\"dados salvos... reiniciarei com a nova config\"}";
        statusCode = 200;
        ESP.reset();
      } else {
        content = "{\"Error\":\"404 not found\"}";
        statusCode = 404;
        Serial.println("Sending 404");
      }
      server.sendHeader("Access-Control-Allow-Origin", "*");
      server.send(statusCode, "application/json", content);
 
    });
  } 
}
